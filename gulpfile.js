'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var eslint  = require('gulp-eslint');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');

gulp.task('lint', function() {
  gulp.src(['./app/**/*.js',  
  './app/js/modules/**/*.js',
  '!./app/bower_components/**'])
  .pipe(eslint({ configFile: './eslint.json' }))
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('clean', function() {
  gulp.src('./dist/*')
    .pipe(clean({force: true}));
  gulp.src('./app/js/bundled.js')
    .pipe(clean({force: true}));
});

gulp.task('minify-css', function() {
  var opts = {comments:true,spare:true};
  gulp.src(['./app/**/*.css', 
  '!./app/bower_components/**'])
  .pipe(minifyCSS(opts))
  .pipe(gulp.dest('./dist/'))
});

gulp.task('minify-js', function() {    
  gulp.src(['./app/**/*.js',
  './app/modules/**/*.js',
  '!./app/bower_components/**'])
  .pipe(uglify({
    // inSourceMap:
    // outSourceMap: "app.js.map"
  }))
  .pipe(gulp.dest('./dist/'));
});

gulp.task('copy-bower-components', function () {
  gulp.src('./app/bower_components/**')
  .pipe(gulp.dest('dist/bower_components'));
});

gulp.task('copy-html-files', function () {
  gulp.src('./app/**/*.html')
  .pipe(gulp.dest('dist/'));
});

gulp.task('connect', function () {
  connect.server({root: 'app/', port: 8888});
});

gulp.task('connectDist', function () {
  connect.server({root: 'dist/', port: 9999});
});

gulp.task('default',['lint', 'connect']);

gulp.task('build', function() {
  runSequence( ['clean'], 
  ['lint', 'minify-css', 'minify-js', 
  'copy-html-files', 'copy-bower-components', 'connectDist']
  );
});