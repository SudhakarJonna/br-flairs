## Wizely

Angular starter featuring Gulp, Bower.

## Quick Start

1. Clone the repo
2. Install the global requirements: `npm install -g gulp bower`
3. Install the local requirements: `npm install`
4. Install the Bower components: `bower install`
5. Run locally: `gulp`
6. Create a build: `gulp build`

Reference link:
https://www.linkedin.com/pulse/http-post-get-web-api-calls-using-angularjs-suresh-muttagi

https://www.jvandemo.com/how-to-configure-your-angularjs-application-using-environment-variables/

https://coderwall.com/p/uqqraq/angular-promises-basics-q-service-for-http

https://chariotsolutions.com/blog/post/angularjs-corner-using-promises-q-handle-asynchronous-calls/

https://www.undefinednull.com/2014/02/25/angularjs-real-time-model-persistence-using-local-storage/