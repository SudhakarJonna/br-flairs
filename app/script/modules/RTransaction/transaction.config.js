'use strict';

var RTransactionModule = angular.module('RTransactionModule', []);

RTransactionModule.config([
  '$stateProvider', 
  '$urlRouterProvider', 
  '$locationProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $stateProvider
    .state('RTransactionModule', {
      url: '/transaction',
      templateUrl: './script/modules/RTransaction/transaction.html',
      controller: 'transactionController'
    });
    $urlRouterProvider.otherwise('/');
  }
]);