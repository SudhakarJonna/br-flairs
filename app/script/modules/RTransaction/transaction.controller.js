'use strict';

angular
  .module('RTransactionModule')
  .controller('transactionController', 
  ['$scope', '$state', 'ATFactory', 'UIDFactory',
  function ($scope, $state, ATFactory, UIDFactory) {
    
    var transactionCtrl = this;

    $scope.transactionSTContent = { };
    
    transactionCtrl.previousDate = previousDate;
    
    $scope.gridOptions = {
      enableSorting: true,
      enableFiltering: true,
      columnDefs: [
        { name:'Field 01', field: 'field01' },
        { name:'Field 02', field: 'field02' },
        { name:'Field 03', field: 'field03' },
        { name:'Field 04', field: 'field04' },
          ],
          data : [      
        {
          'field01': 'sasdaada',
          'field02': 'asddaa',
          'field03': '1',
          'field04': 'a573'
        },{
          'field01': 'xasdaada',
          'field02': 'asddaa',
          'field03': '2',
          'field04': 'a573'
        },{
          'field01': 'dasdaada',
          'field02': 'asddaa',
          'field03': '3',
          'field04': 'a573'
        },{
          'field01': 'hasdaada',
          'field02': 'asddaa',
          'field03': '5',
          'field04': 'a573'
        },
        
      ]
  };

    var starAdminData = [      
      {
        'No': '034',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      }, 
      {
        'No': '035',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      }, 
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      },
      {
        'No': '036',
        'Invoice': 'Designs',
        'Client': 'Client',
        'VatNo': '53275531',
        'Created': '12 May 2017',
        'Price': '$349'
      }  
    ];
    
    $scope.bsTableControl = {
      options: {
        data: starAdminData,
        columns: [{
          field: 'No',
          title: 'No'
        }, {
          field: 'Invoice',
          title: 'Invoice'
        }, {
          field: 'Client',
          title: 'Client'
        },{
          field: 'VatNo',
          title: 'VatNo'
        }, {
          field: 'Created',
          title: 'Created'
        }, {
          field: 'Price',
          title: 'Price'
        }]      
    }
  }
//-------
    /*****
    * CONFIGURATION
    */

    //Main navigation
    $.navigation = $('nav > ul.nav');

    $.panelIconOpened = 'icon-arrow-up';
    $.panelIconClosed = 'icon-arrow-down';

    /****
    * MAIN NAVIGATION
    */

      // Add class .active to current link
      $.navigation.find('a').each(function(){

        var cUrl = String(window.location).split('?')[0];

        if (cUrl.substr(cUrl.length - 1) == '#') {
          cUrl = cUrl.slice(0,-1);
        }

        if ($($(this))[0].href==cUrl) {
          $(this).addClass('active');

          $(this).parents('ul').add(this).each(function(){
            $(this).parent().addClass('open');
          });
        }
      });

      // Dropdown Menu
      $.navigation.on('click', 'a', function(e){

        if ($.ajaxLoad) {
          e.preventDefault();
        }

        if ($(this).hasClass('nav-dropdown-toggle')) {
          $(this).parent().toggleClass('open');
          resizeBroadcast();
        }

      });

      function resizeBroadcast() {

        var timesRun = 0;
        var interval = setInterval(function(){
          timesRun += 1;
          if(timesRun === 5){
            clearInterval(interval);
          }
          window.dispatchEvent(new Event('resize'));
        }, 62.5);
      }

//-------
    function previousDate() {

    };

  }]
);