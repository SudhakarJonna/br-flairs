'use strict';

angular
  .module('loginModule')
  .service('loginApiService', 
  ['$resource', '$http', '$q', 'apiConst', 
  function ($resource, $http, $q, _env) {
    
    var result;
    var deferred      = $q.defer();

    var APIBASE_URI   = apiConst.apiUrl + apiConst.baseUrl;
    var ENDPOINT_URI  = '/authentication/signIn';
    var API_URL       = APIBASE_URI + ENDPOINT_URI;

    var ENDPOINT_HEADER = {
      'Content-Type'    : apiConst.contentType,
      'clientID'        : apiConst.clientID, 
      'clientSecretKey' : apiConst.clientSecretKey
    };

    this.postApiCall = function(objectData) {
      
      $http({
        method  : 'POST',
        url     : API_URL,
        headers : ENDPOINT_HEADER,
        data    : objectData
      })
      .then(function (response) {
        deferred.resolve(response.data);
      })
      .catch(function (response) {
        deferred.reject(response);
      });

      return deferred.promise;
      
    };
  }
]);