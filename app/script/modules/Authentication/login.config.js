'use strict';

var loginModule = angular.module('loginModule', []);

loginModule.config([
  '$stateProvider', 
  '$urlRouterProvider', 
  '$locationProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $stateProvider
    .state('loginModule', {
      url: '/',
      templateUrl: './script/modules/authentication/login.html',
      controller: 'loginController'      
    });
    $urlRouterProvider.otherwise('/');
  }
]);