'use strict';

angular
  .module('loginModule')
  .controller('loginController', 
  ['$scope', '$state', 'loginApiService', 'ATFactory', 
  'UIDFactory', '$location',
  function ($scope, $state, loginApiService, ATFactory, 
  UIDFactory, $location) {
    
    var loginUser = this;

    // Static Content
    $scope.loginSTContent = {
      forgotPassword: 'Forgot Password ?',
      logIn: 'LOGIN',
      errorMessage: {
        requiredUserEmail: 'Required Email',
        wrongEmailPattern: 'Invalid Email',
        requiredPassword: 'Required Password'
      }
    };
    
    // Regrex Patterns
    $scope.emailPatternRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Constructors and Functions
    loginUser.initalClean       = initalClean();
    loginUser.loginSUForm       = loginSUForm;
    loginUser._lsufSuccess      = _lsufSuccess;
    loginUser._lsufNoContent    = _lsufNoContent;
    
    function initalClean() {
      ATFactory.removeToken();
      UIDFactory.removeUserID();      
    };

    function loginSUForm(isValid) {

      var loginData = {
        'email'     : $scope.email,
        'password'  : $scope.password
      };

      loginApiService.postApiCall(loginData)
      .then(function(response) {
    	  
        var statusCode    = response.statusCode;
        var statusMessage = response.statusMessage;
        var responseData  = response.responseData;

        if(statusCode === '200') {
          _lsufSuccess(responseData);
        } else if (statusCode === '204') {
          _lsufNoContent(responseData);
        } else {
          // we think about it later....
        }

      })
      .catch(function(response) {
        console.log("Error Information", response);
      });

    };

    function _lsufSuccess(responseData) {
      ATFactory.setToken(responseData.token);
      UIDFactory.setUserID(responseData.userInfo._id);
      $location.path('/dashboard');
    };

    function _lsufNoContent() {
      // blank as of now.... 
    }

  }]
);