'use strict';

angular
  .module('wizelyApp')
  .factory("ATFactory", 
  ['$window', '$rootScope',
  function ($window, $rootScope) {
    
    var ATFactoryResult = {};

    angular.element($window).on('storage', function(event) {
      if (event.key === 'ATStorage') {
        $rootScope.$apply();
      }
    });

    ATFactoryResult.setToken = function(tokenObject) {
      return $window.localStorage 
        && $window.localStorage.setItem('ATStorage', tokenObject);
    };

    ATFactoryResult.getToken = function() {
      return $window.localStorage 
        && $window.localStorage.getItem('ATStorage');
    };

    ATFactoryResult.removeToken = function() {
      return $window.localStorage 
        && $window.localStorage.removeItem('ATStorage');
    };

    return ATFactoryResult;
    
  }]
);