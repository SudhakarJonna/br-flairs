'use strict';

angular
  .module('dashboardModule')
  .controller('dashboardController', 
  ['$scope', '$state', 'ATFactory', 'UIDFactory',
  '$location',
  function ($scope, $state, ATFactory, UIDFactory,
  $location) {
    
    var dashboardCtrl = this;

    $scope.dashboardSTContent = { 
      'sideMenu': {}
    };

    $scope.$on('$routeChangeStart', function (scope, next, current) {
      if (next.$$route.controller != "Your Controller Name") {
          // Show here for your model, and do what you need**
          $("#yourModel").show();
      }
    });

        
    dashboardCtrl.SMenuRGoal        = SMenuRGoal;
    dashboardCtrl.SMenuRradeem      = SMenuRradeem;
    dashboardCtrl.SMenuRCancel      = SMenuRCancel;
    dashboardCtrl.SMenuRTransaction = SMenuRTransaction;

    function SMenuRGoal() { };

    function SMenuRradeem() { };

    function SMenuRCancel() { };

    function SMenuRTransaction() { 
      console.log("---------")
      $location.path('/transaction');
    };

  }]
);