'use strict';

var dashboardModule = angular.module('dashboardModule', []);

dashboardModule.config([
  '$stateProvider', 
  '$urlRouterProvider', 
  '$locationProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $stateProvider
    .state('dashboardModule', {
      url: '/dashboard',
      templateUrl: './script/modules/Dashboard/dashboard.html',
      controller: 'dashboardController'
    });
    $urlRouterProvider.otherwise('/');
  }
]);