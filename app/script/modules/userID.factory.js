'use strict';

angular
  .module('wizelyApp')
  .factory("UIDFactory", 
  ['$window', '$rootScope',
  function ($window, $rootScope) {
    
    var UIDFactoryResult = {};

    angular.element($window).on('storage', function(event) {
      if (event.key === 'UIDStorage') {
        $rootScope.$apply();
      }
    });
    
    UIDFactoryResult.setUserID = function(userObject) {
      return $window.localStorage
        && $window.localStorage.setItem('UIDStorage', userObject);
    };

    UIDFactoryResult.getUserID = function() {
      return $window.localStorage 
        && $window.localStorage.getItem('UIDStorage');
    };

    UIDFactoryResult.removeUserID = function() {
      return $window.localStorage 
        && $window.localStorage.removeItem('UIDStorage');
    };

    return UIDFactoryResult;
    
  }]
);