'use strict';

var AppModule = [
  'ui.router',
  'ngAnimate',
  'ngResource',
  'loginModule',
  'dashboardModule',
  'RTransactionModule'  
];

var apiConst = {
  apiUrl          : 'http://localhost:3000',
  baseUrl         : '/wizelyAdmin',
  contentType     : 'application/json',
  clientID        : 'wizelyIn',
  clientSecretKey : 'wizelyIndia'
};

var ngModule = angular.module('wizelyApp', AppModule);
ngModule.constant('apiConst', apiConst);